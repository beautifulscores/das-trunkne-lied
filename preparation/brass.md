# Brass

## Horns

Two times two in a `PianoStaff`. Each a default `\partcombine` job.  
(Of course) without key signature.

## Trumpets

Looked until p. 72. The same as horns: 2 + 2, without key signature.

## Trombones and tuba

Only looked at the beginning, but I assume it's the same as usual:

- two trombones in one staff
- bass trombone in second staff
- tuba as a single instrument