# Topics to be discussed before music entry

This document is a work in progress and is open to modification.
Particularly encouraged is the removal of obsolete topics ;-) .

## Score and part structure

As the instruments make use of some heavy *divisi* and staff splitting we have
to make sure before that we can efficiently make use of the entered music.

As for the cello part I have an idea:

- create four parts (as that's the maximum)
- enter music for part 1 first
- enter music for additional parts,  
  adding hyperlinks for all segments where the part doesn't have individual
  music. That should ensure complete parts.
- The score should also have four cello staves (frenched), but I think we'll
  have to manually prepare them by picking the right segments where necessary.

## Cue notes

We should be clear about the fact that we will have to enter cue notes at a
later point. I think we should send the finished parts to the orchestra and
ask someone there to decide about where to enter cue notes in the parts.
Then we have to be able to take care of that painlessly.  
Maybe that's not an issue at all, but I don't know about it, so I prefer being
overly cautious ...

## Order of music entry

There are two aspects to that question:

- we need a MIDI/Audio file until october. So we need to have a complete
  "outline".
- There is so much music in parallel that it makes sense to work in segments.
  That is: one enterer takes one segment and works his way through the wnole
  score.

## Identifying ambiguous elements in the score

e.g. hairpins not aligned consistently

## Use of markup functions

It's important to define rules and "stylesheets" to make the markups
consistent. I think we need a) styles for certain types of markups (e.g.
playing indications) and b) commands to directly enter default stuff (e.g.
"pizz.").

## General coding standards

We should at least decide about authoritative standards concerning

- indentation  
    - (each measure begins at the beginning of a line, 
       everything else should be indented
    - more than one measure on one line is only acceptable
      in the case of multimeasure rests
    - substantial (i.e. long) tweaks or commands should go on a line of its own
- barchecks after each measure
- barnumber comments  
  (I suggest an empty line and a line with only a barnumber comment
   for *every* measure)
- an authoritative list of available commands (from `oll` or from
  a project library

Should we also define more concrete rules? :

- when to use whitespace and when not: e.g. `e4~ e` vs. ` e4 ~ e`
- order of attached items, e.g.
  "note(duration)->articulation->dynamic->beam->tie/slur"

I know this may be intrusive, but I think also that it's important to have
a consistent coding, first for ourselves to be able to read others' code
better, and then when we want to use the project as a reference/demo.


## Define rules how to fill out the templates

## Define how to generate new part templates

I'm afraid the Python script(s) are somewhat messed up.

## Define rules how to deal with issues in the original score

Anybody who notices an issue in the original score should handle that instance
somehow. Obvious errors should be fixed immediately, but only when documented
properly. In doubt one should faithfully copy the original but also document it.
For this `\annotate` has to be fixed to be at least usable for music entry.

## Branching workflow

I think only peer-reviewed content should ever go into `master`.
That means any music entry should be done in a branch and finished with a
pull request. However, work doesn't have to be completely finished for that.
I.e. if I enter five segments of a part I can ask for review.
Probably it would be best if there are teams of two that do their reviews
mutually.  
These branches should be quite short-lived because we want `master` to document
the progress. I think any push to master should trigger a build of the project 
that is then displayed publicly.

## Communication channels

I think we should use a mailing list and an issue tracker, the issue tracker
only for real issues (bugs, persistent questions and TODOs) and the mailing list
for usual questions ("does anybody currently work on the harp part?",
"how do I achieve this?" or "Can somebody confirm this should be a d flat?" etc).

Although in general tools like Trello serve such needs well I think a proper
(and dedicated) mailing list will be good too, without forcing contributors
to sign up with Trello.

Or should we (additionally/alternatively) use a forum? I've installed 
[Vanilla forums](http://vanillaforums.org) on https://git.ursliska.de/forum as
a test 