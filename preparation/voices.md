# Vocal parts

The solo vocals are not an issue with regard to score structure.

Maybe a general question could be how to deal with lyrics: should we
consequently enter lyrics for each voice or should we look for possible
duplications and work with variables? I didn't check that so far, but I could
imagine that becomes complicated when different parts use different text
repetitions (if they do).

## Choir (and choir solo)

- generally eight voices, sometimes only four.
  Each voice written in its own staff, but there are a few occasions where
  it's notated as temporary polyphony (short voice splittings in generally
  four-voice sections).
- Tenor written in tenor clef. I suggest we use tenor clef for entering music
  (that means it would be kept like that when compiling segments) and keep it
  for the proof-reading stage. Only for production we'd print the full score
  in octaviated treble clef.  
  (BTW: in the piano score the tenors use treble clef)

There's one issue (but that shouldn't be ours): in the instrument list it says:
eight choir solo voices for the women each. But in the score (as well as the
piano score) I didn't find any reference to this. So it's not clear when the
solo voices are to be singing.