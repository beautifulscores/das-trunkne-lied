# Analyses of the cello part(s)

- starts with one staff
- splits to two staves p. 8  
  (piano brace around, staffgroup labeled "Vc."
- two staves, divisi in two each (one chord only) p. 15
- two staves, divisi in **three** each, for a few measures, p. 16  
  Voices labeled `I. - II. - II. // III. - III. - IV.`  
  I assume this indicates the different music stands  
  -> This can easily be done in temporary polyphony, but we have to make *sure*  
  that we can make *any* kind of individual part from it.  
  In any case, the numbering doesn't necessarily mean independent *parts*.
- one staff, four-voice chords, label "4-fold" p. 44  
  number of voices changes without indication. So what to do with that?
- splitting from one to two staves on a tied note p. 55-56
- one staff, two voices p. 58  
  (like partcombine, but I think it could simply be written on two staves)
- **four** staves p. 74
- two staves with two voices each p. 75 -> continuing p. 74
- three staves p. 77
- two staves, but the first half of the system unisono, splitting up only
  in between. p. 81
- "Solo" p. 85 (but only a label, I don't think we need a voice for that
- solo plus tutti, but not at the same time. I think this could go into
  one continuous staff
- one staff, divisi in three p. 89
- "Solo" plus two more staves p. 91


Basically it's up to four voices with one single instance of six voices,
but these are explicitly labelled 1-4.

But within this range of voice numbers *any* combination can happen..

So I think we should enter four complete parts and pick the segments to
combinations when composing the parts or the score.

For the score we can simply work with frenched systems, but for the parts we'll
have to decide how to proceed: When there's only one voice in the score that
means that *all* parts should contain that music.