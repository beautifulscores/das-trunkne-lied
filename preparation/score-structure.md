# Analysis of the score structure

In order not to run into annoying/embarrassing issues we have to start with a thorough analysis
of the score structure. What we want is a set-up that allows us to enter the music segments and
then compose them to sensible parts and a score. This should be mostly automatic and allow us to
treat special cases with sufficient ease.

In this document we link to dedicated "sub-files", giving only the shortest summary here.

- [Instrument list](instrument-list.md)

### Strings

- [Cello part(s)](cello.md)  
  (For all other string instruments please refer to the vc document.
  It's the only detailed description.)
- Viola parts  
  The same as with cello: up to four voices.  
  Maybe the solo viola should get its own voice too.
- Double bass  
  Mostly on one staff, partially divisi in two.  
  One section with two staves, should remain like this.
- Violin parts (I and II)  
  are mostly less complex (that is: they are *together* as comples as the other
  parts *alone*. There is only one short section where Vl 1 splits to five
  voices on four staves while Vl 2 is also divisi in two. And there is also 
  the solo violin

A general consideration about the string parts:
As they all are split differently from 1 to four voices I think we should provide
only one single "part" for each "instrument". I think all cello players should
have all cellos parts because it will be necessary to decide on-the-fly how
the parts are divided. (If we prepare four different parts *we* would have to
decide how voices are distributed when the music is only divisi in three.)

Actually this makes it easier for us because we can have the "part" for the
celli (e.g.) identical to their section in the score.

### Vocals

The solo voices don't pose *any* problems regarding the score structure.
The choir is basically eight voices, one `ChoirStaff` with four `PianoStaff`s
in it.  
More in [voices](voices.md)

### Harp

Is a "simple" PianoStaff.

### Percusion

All instruments are on individual staves, the percussion within a `PianoStaff`
(although most of the time there is only one playing at a time).

All instruments are pitched, but I suggest writing cymbals, triangle, tamtam
and bass drum on percussion staves (saves space and makes them more visible).

Timpani are written without key signature, with individual accidentals.

### Woodwinds

[Separate file](woodwinds.md)

### Brass

[Separate file](brass.md)

