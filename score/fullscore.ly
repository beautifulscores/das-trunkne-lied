\version "2.17.16"

%{
   This file is just a stub
   used in developing the 'real' score structure
%}

% Load project-wide libraries, settings and values
\include "init-edition.ily"

% Keep the original page breaking of the handwritten score
%% at least while developing the score
\include "layout/keep-original-breaks-score.ily"


% String parts
\include "../parts/violoncello/part.ily"
\include "../parts/doublebass/part.ily"

% Include temporarily in order to have at least a working score
% (will result in a \part variable with empty segments
%  that can be used in dummy staves)
\include "global/empty-segments.ily"
\include "makescore/concat-segments.ily"

\score {

  \new GrandStaff <<
    
    \new StaffGroup = "Winds" <<
      \new Staff \part
      \new Staff \part
    >>
    
    \new StaffGroup = "Soloists" <<
      \new Staff \part
    >>
    
    \new ChoirStaff = "Choir" <<
      \new Staff \part
    >>
    
    \new StaffGroup = "Strings" <<
      \new Staff \violoncello
      \new Staff \doublebass
    >>
    
  >>
  
  \layout {}
  \midi {}
}