Das trunkne Lied
================

Oskar Fried: Das Trunkene Lied

---

New edition of Oskar Fried "Das Trunkne Lied",  
commissioned by: [„das junge orchester NRW“](www.djo-nrw.de)

---

For installation and other notes please refer to the
[Wiki](https://git.ursliska.de/beautifulscores/das-trunkne-lied/wikis/Home.md) pages

---

Contact:  
Urs Liska <mailto:ul@beautifulscores.net>  
Mailing list:  http://lists.ursliska.de/cgi-bin/mailman/listinfo/das-trunkne-lied
