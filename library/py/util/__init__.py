#!/usr/bin/env python

import os, sys

def check_path():
    path,  name = os.path.split(sys.argv[0])
    os.chdir(os.path.join(path, '..', '..'))
    print os.getcwd()
    if not os.path.exists('.trunkneslied'):
        print 'Path:', os.getcwd()
        print 'does not look like the root dir of project'
        print 'Trunknes Lied.'
        print 'Please check location of script'
        print sys.argv[0]
        exit(1)
