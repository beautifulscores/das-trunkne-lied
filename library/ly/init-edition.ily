\version "2.17.3"

#(ly:set-option 'relative-includes #t)
#(define-public editionInitialized #t)

% Load library functions

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions from openlilylib

% Git functions for status references
\include "general-tools/git-commands/definitions.ily"
% Easy option to create on-the-fly dynamics/text combinations
\include "input-shorthands/easy-custom-dynamics/definitions.ily"

\include "layout/global-layout.ily"

% Select whether to keep or discard the line
% and page breaks of the original score
% (may be overwritten individually for each part)
\include "layout/keep-original-breaks.ily"
%\include "layout/discard-original-breaks.ily"

\include "global/headers.ily"

% Include markup styles
\include "styles/markups.ily"

% Include lilypond-doc library (has to be in the path
\include "functions/annotate.ily"

% Include shorthands
\include "functions/shorthands.ily"
