\version "2.17.3"

\header {
  title = "Das trunkne Lied"
  subtitle = "(aus dem Zarathustra von Friedrich Nietzsche)"
    %{CR
      @mm: "Title"
      @src: @score:
      @cmt: "Dot after “Nietzsche”"
    %}
  subsubtitle = "Für Soli, Chor und Orchester"
    %{CR
      @mm: "Title"
      @src: @score:
      @cmt: "Comma after “Soli“ missing"
    %}

  composer = "Oskar Fried, Op. 11"
  copyright = \markup \concat {
    "Engraving based on "
    \gitIsCleanMarkup "(clean) " "(dirty) "
    "commit "
    \gitCommitish
    ", "
    \gitDateTime
    " - "
    \gitAuthor
  }
}
