%{
   library/ly/global/empty-segments.ily
   
   Define the complete time structure of the piece
   Each segment is named with a roman numeral index and 
   contains the empty measures, original line and page 
   breaks nad the rehearsal numbers.
   Each segment ends with the rehearsal mark corresponding
   to its name:
   segment I = 'null' up to rehearsal mark '1'
%}

\version "2.17.3"

I = {
  \time 3/4
  R2.*10 \origBreak %3 [these numbers indicate the number of the previous page]
  \mark \default
}

II = {
  \time 3/4
  R2.*5 \time 5/4 R4*5 \time 3/4
  \mark \default
}

III = {
  \time 3/4
  R2.*3 \origBreak | %4
  R2.*8
  \mark \default
}

IV = {
  \time 3/4
  R2.*3 \origBreak | %5
  R2.*7
  \mark \default
}

V = {
  \time 3/4
  R2.*3 \origBreak | %6
  R2.*5
  \mark \default
}

VI = {
  \time 3/4
  R2.*3 \origBreak | %7
  R2.*4
  \mark \default
}

VII = {
  \time 3/4
  R2.*3 \origBreak | %8
  \time 5/4 R4*5 \time 3/4 R2.*5 \origBreak | %9
  \mark \default
}

VIII = {
  \time 3/4
  R2.*4 \time 5/4 R4*5
  \mark \default
}

IX = {
  \time 3/4 R2. \origBreak | %10
  R2.*3 \time 5/4 R4*5
  \mark \default
}

X = {
  \time 3/4 R2. \origBreak | %11
  R2.*5 \origBreak | %12
  R2.*4
  \mark \default
}

XI = {
  \time 3/4
  R2.*2 \origBreak | %13
  R2.*8 \origBreak | %14
  \mark \default
}

XII = {
  \time 4/4 R1*5 \origBreak | %15
  \mark \default
}

XIII = {
  \time 4/4
  R1*4
  \mark \default
}

XIV = {
  \time 4/4
  R1*5 \origBreak | %16
  R1*4 \mark \default
}

XV = {
  \time 4/4
  R1*2 \origBreak | %17
  \time 5/4 R4*5 \time 4/4 R1
  \mark \default
}

XVI = {
  \time 4/4
  R1*2 \origBreak | %18
  R1*4 \origBreak | %19
  R1*2
  % Rehearsal mark is missing in the score
  % -> taken from piano reduction.
  \mark \default
}

XVII = {
  \time 4/4
  R1 \origBreak | %20
  R1*3 \origBreak | %21
  R1*2
  \mark \default
}

XVIII = {
  \time 4/4
  R1*3 \origBreak | %22
  R1
  \mark \default
}

XIX = {
  \time 4/4
  R1*3 \origBreak | %23
  R1
  \mark \default
}

XX = {
  \time 4/4
  R1*3 \origBreak | %24
  R1*3 \origBreak | %25
  R1*2
  \mark \default
}

XXI = {
  \time 4/4
  R1*2 \origBreak | %26
  R1*6 \origBreak | %27
  \mark \default
}

XXII = {
  \time 4/4
  R1*5 \origBreak | %28
  R1 \time 3/2 R1. \time 4/4 R1
  \mark \default
}

XXIII = {
  \time 4/4
  R1 \origBreak | %29
  R1*4 \origBreak | %30
  R1*3
  \mark \default
}

XXIV = {
  \time 4/4
  R1*5 \origBreak | %31
  R1*5
  \mark \default
}

XXV = {
  \time 4/4
  R1*2 \origBreak | %32
  R1*3 \origBreak | %33
  R1*3 \origBreak | %34
  R1*3 \origBreak | %35
  R1*3 \origBreak | %36
  \mark \default
}

XXVI = {
  \time 3/4 R2.*5 \origBreak | %37
  R2.*5
  \mark \default
}

XXVII = {
  \time 3/4
  R2. \origBreak | %38
  R2.*5 \origBreak | %39
  R2.*5 \origBreak | %40
  R2.
  \mark \default
}

XXVIII = {
  \time 3/4
  R2.*4 \origBreak | %41
  R2.*6 \origBreak | %42
  R2.
  \mark \default
}

XXIX = {
  \time 3/4
  R2.*8 \origBreak | %43
  R2.*4
  \mark \default
}

XXX = {
  \time 3/4
  R2.*4 \origBreak | %44
  R2.*4 \origBreak | %45
  R2.
  \mark \default
}

XXXI = {
  \time 3/4
  R2.*4 \origBreak | %46
  R2.*8 \origBreak | %47
  \mark \default
}

XXXII = {
  \time 3/4
  R2.*7 \origBreak | %48
  \mark \default
}

XXXIII = {
  \time 3/4
  R2.*5 \origBreak | %49
  R2.*4
  \mark \default
}

XXXIV = {
  \time 3/4
  R2. \origBreak | %50
  R2.*6 \origBreak | %51
  R2.*4
  \mark \default
}

XXXV = {
  \time 3/4
  R2.*5 \origBreak |%52
  \mark \default
}

XXXVI = {
  \time 12/8 R1.*2 \origBreak |%53
  \time 6/8 R2. \time 12/8 R1.*2 \origBreak |%54
  \mark \default
}

XXXVII = {
  \time 12/8
  R1.*2 \time 6/8 R2. \origBreak |%55
  \time 12/8 R1.*2 \time 5/8 R8*5 \origBreak |%56
  \mark \default
}

XXXVIII = {
  \time 6/8 R2.*5 \origBreak |%57
  R2.*2 \time 12/8 R1. \origBreak |%58
  \mark \default
}

XXXIX = {
  \time 12/8
  R1.*2 \origBreak |%59
  R1.*2 \origBreak |%60
  \time 6/8 R2.
  \mark \default
}

XL = {
  \time 12/8 R1.*2 \origBreak |%61
  R1.*3
  \mark \default
}

XLI = {
  \time 12/8
  R1. \origBreak |%62
  R1.*2 \origBreak |%63
  \time 9/8 R8*9 \time 12/8 R1. \origBreak |%64
  R1.*2 \origBreak |%65
  \mark \default
}

XLII = {
  \time 12/8
  R1.*2  \origBreak |%66
  \time 9/8 R8*9 \time 12/8 R1. \origBreak |%67
  \mark \default
}

XLIII = {
  \time 3/4 R2.*4 \origLineBreak | %(68)
  R2.*6 \origBreak |%68
  R2.*7 \origBreak |%69
  R2.*2
  \mark \default
}

XLIV = {
  \time 3/4
  R2.*3 \origBreak |%70
  R2.*2
  \mark \default
}

XLV = {
  \time 3/4
  R2.*5 \origBreak |%71
  R2.*7 \origBreak |%72
  R2. \time 4/4 R1
  \mark \default
}

XLVI = {
  \time 3/4 R2.*3 \origBreak |%73
  \time 4/4 R1 \time 5/4 R4*5 \time 3/4 R2.*2 \origLineBreak |%(74)
  R2. \time 5/4 R4*5
  \mark \default
}

XLVII = {
  \time 3/4 R2. \time 4/4 R1 \origBreak |%74
  \time 3/4 R2.*8 \origBreak |%75
  R2.
  \mark \default
}

XLVIII = {
  \time 3/4
  R2.*5 \origBreak |%76
  R2.*4 \origBreak |%77
  \mark \default
}

XLIX = {
  \time 3/4
  R2.*5 \origBreak |%78
  R2.
  \mark \default
}

L = {
  \time 3/4
  R2.*9 \origBreak |%79
  R2.*3
  \mark \default
}

LI = {
  \time 3/4
  R2.*5 \origLineBreak | %(80)
  R2.*9 \origBreak |%80
  R2.*3 \time 5/4 R4*5
  \mark \default
}

LII = {
  \time 3/4 R2.*2 \origLineBreak
  R2.*5 \time 5/4 R4*5 \origBreak |%81

  \mark \default
}

LIII = {
  \time 3/4 R2.*5 \origLineBreak
  R2.*6 \origBreak |%82
  R2.*6 \origLineBreak
  R2.*6 \origBreak |%83
  \time 5/4 R4*5
  \mark \default
}

LIV = {
  \time 4/4 R1*4 \origBreak |%84
  R1*6 \origLineBreak
  R1*5 \origBreak |%85
  R1*4 \origLineBreak
  R1*5 \origBreak |%86
  R1*5 \origBreak |%87
  \mark \default
}

LV = {
  \time 4/4
  R1*5 \origBreak |%88
  R1*6 \origBreak |%89
  \mark \default
}

LVI = {
  \time 4/4
  R1*6 \origBreak |%90
  R1*7
  \mark \default
}

LVII = {
  \time 4/4
  R1*2 \origBreak |%91
  R1 \time 5/4 R4*5 \time 4/4 R1*5 \origBreak |%92
  R1*3 \time 3/2 R1. \time 5/4 R4*5
  \mark \default
}

LVIII = {
  \time 4/4 R1 \origBreak |%93
  R1*6 \origBreak |%94
  R1*7 \origBreak |%95
  R1*3
  \mark \default
}

LIX = {
  \time 4/4
  R1*4 \origBreak |%96
  R1*4
  \mark \default
}

LX = {
  \time 3/2 R1. \origBreak |%97
  R1.*4 \origBreak |%98
  \time 6/4 R1. \time 3/2 R1. \time 6/4 R1. \time 3/2 R1. \origBreak |%99
  \mark \default
}

LXI = {
  \time 3/2
  R1.*3 \origBreak |%100
  \time 6/4 R1. \time 3/2 R1. \time 6/4 R1. \time 3/2 R1. \origBreak |%101
  \time 6/4 R1. \time 3/2 R1. \time 6/4 R1. \origBreak |%102
  \time 3/2 R1.
  \mark \default
}

LXII = {
  \time 6/4 R1.*3 \origBreak |%103
  R1.*3
  \mark \default
}

LXIII = {
  \time 3/2 R1. \origBreak |%104
  R1.*4 \origBreak |%105
  R1.*2 \time 6/4 R1. \origBreak |%106
  \mark \default
}

LXIV = {
  \time 3/2 R1. \time 6/4 R1. \time 3/2 R1. \origBreak |%107
  \time 6/4 R1.*3 \origBreak |%108
  R1.*2
  \mark \default
}

LXV = {
  \time 3/2 R1.*2 \origBreak |%109
  R1.*3
  \mark \default
}

LXVI = {
  \time 4/4 R1*2 \origBreak |%110
  R1*6 \origBreak |%111
  R1*5 \origBreak |%112
  \mark \default
}

LXVII = {
  \time 4/4
  R1*8 \origBreak |%113
  \mark \default
}

LXVIII = {
  \time 3/4 R2.*6 \origBreak |%114
  R2.*5 \origBreak |%115
  R2.*5 \origBreak |%116
  \mark \default
}

LXIX = {
  \time 3/4
  R2.*5 \origBreak |%117
  \mark \default
}

LXX = {
  \time 3/4
  R2.*4 \origBreak |%118
  R2.*4
  \mark \default
}

LXXI = {
  \time 3/4
  R2. \origBreak |%119
  R2.*4 \origBreak |%120
  R2.
  \mark \default
}

LXXII = {
  \time 3/4
  R2.*3 \origBreak |%121
  R2.*3
  \mark \default
}

LXXIII = {
  \time 3/4
  R2. \origBreak |%122
  R2.*4 \origBreak |%123
  R2.
  \mark \default
}

LXXIV = {
  \time 3/4
  R2.*3 \origBreak |%124
  R2.*2
  \mark \default
}

LXXV = {
  \time 3/4
  R2.*2 \origBreak |%125
  R2. \time 2/4 R2
  \mark \default
}

LXXVI = {
  \time 3/4 R2.*5 \origBreak |%126
  R2.*8 \origBreak |%127
  \time 4/4 R1 \time 3/4 R2.*2
  \mark \default
}

LXXVII = {
  \time 3/4
  R2.*5 \origBreak |%128
  R2.*2 \time 2/4 R2
  \mark \default
}

LXXVIII = {
  \time 3/4 R2.*4 \origBreak |%129
  R2.*7
  \mark \default
}

LXXIX = {
  \time 3/4
  R2. \origBreak |%130
  R2.*2 \time 5/4 R4*5 \origBreak |%131
  \time 3/4 R2.
  \mark \default
}

LXXX = {
  \time 3/4
  R2.*7 \origBreak |%132
  R2.*4
  \mark \default
}

LXXXI = {
  \time 3/4
  R2.*3 \origBreak |%133
  \time 2/4 R2 \time 3/4 R2.*4
  \mark \default
}

LXXXII = {
  \time 3/4
  R2.*2 \origBreak |%134
  R2. \time 2/4 R2 \time 3/4 R2.*5 \origBreak |%135
  \time 5/4 R4*5 \time 3/4 R2.*2
  \mark \default
}

LXXXIII = {
  \time 3/2 R1. \time 3/4 R2.*3 \origBreak |%136
  \mark \default
}

LXXXIV = {
  \time 3/4
  R2.*6 \time 4/4 R1*2 \origBreak |%137
  \mark \default
}

LXXXV = {
  \time 3/4 R2.*4 \origBreak |%138
  R2.*4 \origBreak |%139
  R2.*2
  \mark \default
}

LXXXVI = {
  \time 3/4
  R2.*3 \origBreak |%140
  R2.
  \mark \default
}

LXXXVII = {
  \time 3/4
  R2.*3 \origBreak |%141
  R2.*4 \origBreak |%142
  R2.*2 \time 5/4 R4*5
  \mark \default
}

LXXXVIII = {
  \time 3/4 R2. \origBreak |%143
  R2.*4 \origBreak |%144
  \mark \default
}

LXXXIX = {
  \time 3/4
  R2.*7 \origBreak |%145
  \mark \default
}

XC = {
  \time 3/4
  R2.*5 \bar "|." |%146
}

pagenum = #3