\version "2.17.3"

%{
  Scheme engraver that places double bar lines before time signature changes
  Provided by Jan-Peter Voigt
  Presumably written by David Nalesnik
%}

doubleBarlinesAfterTimeSig = #(lambda (context)
          (let ((time-signature '())
                (last-fraction #f))

               `((process-music
                  . ,(lambda (trans)
                          (let ((frac (ly:context-property context 'timeSignatureFraction)))
                               (if (and (null? time-signature)
                                        (not (equal? last-fraction frac))
                                        (fraction? frac))
                                   (begin
                                     (ly:context-set-property! context 'whichBar "||")
                                     (set! last-fraction frac)
                          )))
                ))

                (stop-translation-timestep
                  . ,(lambda (trans)
                          (set! time-signature '()))))
)) 

%{ Usage:
  
music = {
  \time 4/4
  R1
  \time 3/4
  R2.
  \time 4/4
  R1
}

\score {
  \new Staff \music
  
  \layout {
    \context {
      \Score
      \consists #doubleBarlinesAfterTimeSig
    }
  }
}

%}