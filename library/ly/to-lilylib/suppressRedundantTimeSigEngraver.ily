%{
   Engraver that suppresses the printing of a
   time signature if it is the same that is 
   currently in effect.
   
   Author: David Nalesnik
   http://www.mail-archive.com/lilypond-user@gnu.org/msg72628.html
   
   This is actually a work-around to an unwanted behaviour, see
   http://lists.gnu.org/archive/html/lilypond-user/2013-04/msg00851.html and
   https://lists.gnu.org/archive/html/bug-lilypond/2013-04/msg00162.html
%}

\version "2.14.2"

suppressRedundantTimeSig =
#(lambda (ctx)
  (let ((time-sig '()))

   `((acknowledgers
      (time-signature-interface
       . ,(lambda (engraver grob source-engraver)
           (set! time-sig (cons grob time-sig)))))

     (finalize
      . ,(lambda (trans)
          (reduce
            (lambda (elem prev)
              (if (equal? (ly:grob-property elem 'fraction)
                          (ly:grob-property prev 'fraction))
                  (begin
                    (ly:grob-suicide! elem)
                    prev)
                  elem))
              '()
              (reverse time-sig))
            (set! time-sig '()))))))

%{ Usage:
 
 music = {
   \time 3/4
  R2.
  \time 4/4
  R1
  \time 4/4
  R1
  \time 3/4
  R2.
}

\score {
  \new Staff \music

  \layout {
    \context {
      \Score
      \consists #suppressRedundantTimeSig
    }
  }
}

%}