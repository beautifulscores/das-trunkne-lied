\version "2.18.2"

%{
  Markup command for local performance indications:
  - technical indications (sordini, change ...)
  - expressive indications
  usage: \markup \techind "Text" or \markup \exprind "Text"
%}
%{
#(define-markup-command (techind layout props text) (markup?)
  "Print an technical performance indication (like sordini or instrument change."
  (interpret-markup layout props
    (markup #:italic
            #:with-color (x11-color "blue")
            text)))

#(define-markup-command (exprind layout props text) (markup?)
  "Print an expressive performance indication."
  (interpret-markup layout props
    (markup #:italic
            #:with-color (x11-color "brown")
            text)))
%}

% Generic markup functions, used as "style sheets"
% Use with a string as argument

% Instructions, mostly with regard to instrument or damper changes
instr =
#(define-scheme-function (parser location text) (markup?)
   #{
     \markup \bold \with-color #(x11-color "magenta") #text
   #})

% Performance indications, playing techniques
perf =
#(define-scheme-function (parser location text) (markup?)
   #{
     \markup \italic \with-color #(x11-color "blue") #text
   #})

% Expressive indications
expr =
#(define-scheme-function (parser location text) (markup?)
   #{
     \markup  \with-color #(x11-color "brown") #text
   #})

% Dynamic text
dyntxt =
#(define-scheme-function (parser location text) (markup?)
   #{
     \markup \italic \with-color #(x11-color "red") #text
   #})

% Specific markup functions, used for ensuring consistent writing
% for repeatedly needed indications

%%%%%%%%%%%%%%%%%
% \instr commands

% con sordino
mit-daempfer = \instr "mit Dämpfer."
% mount damper
daempfer-auf = \instr "Dämpfer auf."
% senza sordino
ohne-daempfer = \instr "ohne Dämpfer."
% remove sordino
daempfer-ab = \instr "Dämpfer ab."

% pizzicato
pizz = \instr "pizz."
% Use bow
bogen = \instr "Bg."

% divisi
geteilt = \instr "get."

%%%%%%%%%%%%%%%%
% \perf commands

% sul ponticello
am-steg = \perf "am Steg."
gedaempft = \perf "gedämpft."
gestopft = \perf "gestopft."
offen = \perf "offen."
stacc = \perf "stacc."

%%%%%%%%%%%%%%%%
% \expr commands

ausdr = \expr "ausdrucksvoll."
espr = \expr "espr."
warm = \expr "warm."
zart = \expr "zart."

