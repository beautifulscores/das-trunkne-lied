%{
   library/ly/layout/discard-original-breaks.ily
   
   map \origBreak and \origLineBreak to empty functions to
   discard the original page layout of the score while entering music
   
   The manual breaks are only entered in empty-segments.ily
   because that is used in each part or score anyway.
   
%}

origBreak = {}

origLineBreak = {}
