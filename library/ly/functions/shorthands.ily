\version "2.18.2"

%{
  This file is part of the "Das trunkne Lied" project library.
  Provides shorthands for simplifying music entry
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dynamics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Switch alignment of hairpins to barlines on/off
% (\hpToBarline ##t causes the hairpin to stop before the barline)
hpToBarline =
#(define-music-function (parser location active)(boolean?)
   #{
     \once \override Hairpin.to-barline = #active
   #})
