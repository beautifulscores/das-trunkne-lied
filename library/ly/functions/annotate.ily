% Develop the interface to \annotate
% See https://github.com/openlilylib/lilypond-doc/wiki/Documenting-musical-content for discussion

\version "2.17.18"

annotate =
#(define-music-function (parser location properties item)
   (ly:context-mod? symbol-list-or-music?)
   ;; annotates a musical object for use with lilypond-doc

   ; define and preset known keys
   (define type "annotation")
   (define context "")
   (define source "")
   (define author "")
   (define date "")
   (define message "No message defined!")

   (define (process-args mod)
     (define cmd (car mod))
     (define key-value (cdr mod))
;     (if (string=? "assign" symbol->string(car mod))
     (begin

;      (display key-value)
      (newline)))
;     (newline)))

   (ly:input-message location (string-append "Type: " type))

   (let ((arguments (ly:get-context-mods properties)))
     (map process-args arguments))

   ; Dummy coloring
     #{
       \once \override #item #'color = #magenta
     #}
   )

