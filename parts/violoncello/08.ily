%{
  parts/violoncello/08.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 08 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

VIII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*4 \time 5/4 R4*5
  \mark \default
}

\compileSegment \VIII
