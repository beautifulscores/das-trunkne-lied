%{
  parts/violoncello/60.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 60 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LX = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/2 R1. \origBreak |%97
  R1.*4 \origBreak |%98
  \time 6/4 R1. \time 3/2 R1. \time 6/4 R1. \time 3/2 R1. \origBreak |%99
  \mark \default
}

\compileSegment \LX
