%{
  parts/violoncello/73.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 73 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LXXIII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2. \origBreak |%122
  R2.*4 \origBreak |%123
  R2.
  \mark \default
}

\compileSegment \LXXIII
