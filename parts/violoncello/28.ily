%{
  parts/violoncello/28.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 28 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XXVIII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*4 \origBreak | %41
  R2.*6 \origBreak | %42
  R2.
  \mark \default
}

\compileSegment \XXVIII
