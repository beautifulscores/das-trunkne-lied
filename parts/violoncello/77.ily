%{
  parts/violoncello/77.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 77 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LXXVII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*5 \origBreak |%128
  R2.*2 \time 2/4 R2
  \mark \default
}

\compileSegment \LXXVII
