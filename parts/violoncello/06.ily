%{
  parts/violoncello/06.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 06 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

VI = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*3 \origBreak | %7
  R2.*4
  \mark \default
}

\compileSegment \VI
