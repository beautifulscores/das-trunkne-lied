%{
  parts/violoncello/58.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 58 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LVIII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4 R1 \origBreak |%93
  R1*6 \origBreak |%94
  R1*7 \origBreak |%95
  R1*3
  \mark \default
}

\compileSegment \LVIII
