%{
  parts/violoncello/33.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 33 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XXXIII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*5 \origBreak | %49
  R2.*4
  \mark \default
}

\compileSegment \XXXIII
