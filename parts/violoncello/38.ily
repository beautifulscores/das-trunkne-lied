%{
  parts/violoncello/38.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 38 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XXXVIII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 6/8 R2.*5 \origBreak |%57
  R2.*2 \time 12/8 R1. \origBreak |%58
  \mark \default
}

\compileSegment \XXXVIII
