%{
  parts/violoncello/03.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 03 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

III = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*3 \origBreak | %4
  R2.*8
  \mark \default
}

\compileSegment \III
