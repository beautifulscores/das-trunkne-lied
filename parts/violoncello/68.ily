%{
  parts/violoncello/68.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 68 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LXVIII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4 R2.*6 \origBreak |%114
  R2.*5 \origBreak |%115
  R2.*5 \origBreak |%116
  \mark \default
}

\compileSegment \LXVIII
