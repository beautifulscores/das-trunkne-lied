%{
  parts/violoncello/72.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 72 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LXXII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*3 \origBreak |%121
  R2.*3
  \mark \default
}

\compileSegment \LXXII
