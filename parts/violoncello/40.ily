%{
  parts/violoncello/40.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 40 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XL = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 12/8 R1.*2 \origBreak |%61
  R1.*3
  \mark \default
}

\compileSegment \XL
