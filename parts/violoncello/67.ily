%{
  parts/violoncello/67.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 67 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LXVII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*8 \origBreak |%113
  \mark \default
}

\compileSegment \LXVII
