%{
  parts/violoncello/63.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 63 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LXIII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/2 R1. \origBreak |%104
  R1.*4 \origBreak |%105
  R1.*2 \time 6/4 R1. \origBreak |%106
  \mark \default
}

\compileSegment \LXIII
