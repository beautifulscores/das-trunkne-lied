%{
  parts/violoncello/57.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 57 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LVII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*2 \origBreak |%91
  R1 \time 5/4 R4*5 \time 4/4 R1*5 \origBreak |%92
  R1*3 \time 3/2 R1. \time 5/4 R4*5
  \mark \default
}

\compileSegment \LVII
