%{
  parts/violoncello/56.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 56 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LVI = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*6 \origBreak |%90
  R1*7
  \mark \default
}

\compileSegment \LVI
