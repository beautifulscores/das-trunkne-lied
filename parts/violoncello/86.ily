%{
  parts/violoncello/86.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 86 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LXXXVI = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*3 \origBreak |%140
  R2.
  \mark \default
}

\compileSegment \LXXXVI
