%{
  parts/violoncello/74.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 74 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LXXIV = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*3 \origBreak |%124
  R2.*2
  \mark \default
}

\compileSegment \LXXIV
