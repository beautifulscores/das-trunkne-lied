%{
  parts/violoncello/54.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 54 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LIV = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4 R1*4 \origBreak |%84
  R1*6 \origLineBreak
  R1*5 \origBreak |%85
  R1*4 \origLineBreak
  R1*5 \origBreak |%86
  R1*5 \origBreak |%87
  \mark \default
}

\compileSegment \LIV
