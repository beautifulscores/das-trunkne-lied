%{
  parts/violoncello/20.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 20 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XX = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*3 \origBreak | %24
  R1*3 \origBreak | %25
  R1*2
  \mark \default
}

\compileSegment \XX
