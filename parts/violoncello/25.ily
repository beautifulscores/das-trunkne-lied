%{
  parts/violoncello/25.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 25 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XXV = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*2 \origBreak | %32
  R1*3 \origBreak | %33
  R1*3 \origBreak | %34
  R1*3 \origBreak | %35
  R1*3 \origBreak | %36
  \mark \default
}

\compileSegment \XXV
