%{
   parts/violoncello.ly

   Standalone file to compile part:

      Violoncello.
      ============

  This is a generated file. Do not edit.

%}

% Load project-wide libraries, settings and values
\include "init-edition.ily"

% Add instrument specific header entries
\header {
  instrument = "Violoncello."
}

% Include layout defaults for instrumental parts
\include "layout/part-layout.ily"

% Include music definition (\part variable)
\include "violoncello/part.ily"

% Include default settings and a score block
\include "makescore/single-part.ily"
