%{
  parts/doublebass/81.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 81 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LXXXI = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*3 \origBreak |%133
  \time 2/4 R2 \time 3/4 R2.*4
  \mark \default
}

\compileSegment \LXXXI
