%{
  parts/doublebass/64.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 64 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LXIV = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/2 R1. \time 6/4 R1. \time 3/2 R1. \origBreak |%107
  \time 6/4 R1.*3 \origBreak |%108
  R1.*2
  \mark \default
}

\compileSegment \LXIV
