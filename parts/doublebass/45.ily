%{
  parts/doublebass/45.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 45 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XLV = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*5 \origBreak |%71
  R2.*7 \origBreak |%72
  R2. \time 4/4 R1
  \mark \default
}

\compileSegment \XLV
