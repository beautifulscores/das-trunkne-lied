%{
  parts/doublebass/07.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 07 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

VII = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*3 \origBreak | %8
  \time 5/4 R4*5 \time 3/4 R2.*5 \origBreak | %9
  \mark \default
}

\compileSegment \VII
