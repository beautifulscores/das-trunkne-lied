%{
  parts/doublebass/75.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 75 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LXXV = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*2 \origBreak |%125
  R2. \time 2/4 R2
  \mark \default
}

\compileSegment \LXXV
