%{
  parts/doublebass/41.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 41 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XLI = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 12/8
  R1. \origBreak |%62
  R1.*2 \origBreak |%63
  \time 9/8 R8*9 \time 12/8 R1. \origBreak |%64
  R1.*2 \origBreak |%65
  \mark \default
}

\compileSegment \XLI
