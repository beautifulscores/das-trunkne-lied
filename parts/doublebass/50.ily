%{
  parts/doublebass/50.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 50 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

L = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*9 \origBreak |%79
  R2.*3
  \mark \default
}

\compileSegment \L
