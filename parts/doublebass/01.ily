%{
  parts/doublebass/01.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 01 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

I = \relative es, {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  \key ges \major
  \clef bass
  \time 3/4
  es2(\p\<^\markup "mit Dämpfer." ges4 | %1
  f2.)\> | %2
  R2.*8\! \origBreak %3 [these numbers indicate the number of the previous page]
  \mark \default
}

\compileSegment \I
