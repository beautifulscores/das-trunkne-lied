%{
  parts/doublebass/68.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 68 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LXVIII = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4 R2.*6 \origBreak |%114
  R2.*5 \origBreak |%115
  R2.*5 \origBreak |%116
  \mark \default
}

\compileSegment \LXVIII
