%{
  parts/doublebass/19.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 19 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XIX = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*3 \origBreak | %23
  R1
  \mark \default
}

\compileSegment \XIX
