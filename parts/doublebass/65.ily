%{
  parts/doublebass/65.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 65 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LXV = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/2 R1.*2 \origBreak |%109
  R1.*3
  \mark \default
}

\compileSegment \LXV
