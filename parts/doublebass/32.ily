%{
  parts/doublebass/32.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 32 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XXXII = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*7 \origBreak | %48
  \mark \default
}

\compileSegment \XXXII
