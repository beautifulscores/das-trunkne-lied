%{
  parts/doublebass/62.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 62 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LXII = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 6/4 R1.*3 \origBreak |%103
  R1.*3
  \mark \default
}

\compileSegment \LXII
