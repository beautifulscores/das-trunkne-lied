%{
  parts/doublebass/48.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 48 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XLVIII = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*5 \origBreak |%76
  R2.*4 \origBreak |%77
  \mark \default
}

\compileSegment \XLVIII
