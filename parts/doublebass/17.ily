%{
  parts/doublebass/17.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 17 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XVII = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1 \origBreak | %20
  R1*3 \origBreak | %21
  R1*2
  \mark \default
}

\compileSegment \XVII
