%{
  parts/doublebass/43.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 43 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XLIII = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4 R2.*4 \origLineBreak | %(68)
  R2.*6 \origBreak |%68
  R2.*7 \origBreak |%69
  R2.*2
  \mark \default
}

\compileSegment \XLIII
