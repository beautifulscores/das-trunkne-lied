%{
  parts/doublebass/46.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 46 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XLVI = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4 R2.*3 \origBreak |%73
  \time 4/4 R1 \time 5/4 R4*5 \time 3/4 R2.*2 \origLineBreak |%(74)
  R2. \time 5/4 R4*5
  \mark \default
}

\compileSegment \XLVI
