%{
  parts/doublebass/35.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 35 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XXXV = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*5 \origBreak |%52
  \mark \default
}

\compileSegment \XXXV
