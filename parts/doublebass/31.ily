%{
  parts/doublebass/31.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 31 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XXXI = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*4 \origBreak | %46
  R2.*8 \origBreak | %47
  \mark \default
}

\compileSegment \XXXI
