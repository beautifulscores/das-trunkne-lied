%{
  parts/doublebass/22.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 22 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XXII = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*5 \origBreak | %28
  R1 \time 3/2 R1. \time 4/4 R1
  \mark \default
}

\compileSegment \XXII
