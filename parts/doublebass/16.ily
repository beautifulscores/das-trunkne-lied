%{
  parts/doublebass/16.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 16 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XVI = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*2 \origBreak | %18
  R1*4 \origBreak | %19
  R1*2
  % Rehearsal mark is missing in the score
  % -> taken from piano reduction.
  \mark \default
}

\compileSegment \XVI
