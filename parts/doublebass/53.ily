%{
  parts/doublebass/53.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 53 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LIII = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4 R2.*5 \origLineBreak
  R2.*6 \origBreak |%82
  R2.*6 \origLineBreak
  R2.*6 \origBreak |%83
  \time 5/4 R4*5
  \mark \default
}

\compileSegment \LIII
