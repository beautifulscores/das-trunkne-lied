%{
  parts/doublebass/05.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 05 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

V = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*3 \origBreak | %6
  R2.*5
  \mark \default
}

\compileSegment \V
