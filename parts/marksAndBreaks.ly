\version "2.17.16"


\include "initEdition.ily"

\include "makescore/concatSegments.ily"

\score {
  \new Staff {
    \set Score.markFormatter = #format-mark-box-numbers
    \set Staff.instrumentName = "Struktur"
    \part
  }
}