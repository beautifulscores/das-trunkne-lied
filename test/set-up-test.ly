\version "2.19.11"

% Load project-wide libraries, settings and values
\include "init-edition.ily"

% Test the different markup commands

\paper {
  indent = 0
}

\markup "Markup commands"

\relative c' {
  c1-\mit-daempfer
  c-\daempfer-auf
  c-\ohne-daempfer
  c-\daempfer-ab
  \break
  c-\pizz
  c-\bogen
  c-\geteilt
  \break
  c-\am-steg
  c-\gedaempft
  c-\offen
  c-\stacc
  \break
  c-\ausdr
  c-\espr
  c-\warm
  c-\zart
}

\markup "Test \\annotate"

\relative c' {
 \annotate \with {
    type = "critical-remark"
    context = "vc1"
    source = "OE"
    author = "Urs Liska"
    date = "2013-06-10"
    message = "Natural g, missing"
  } Accidental
  cis
  cis
 \annotate \with {
    type = "todo"
    context = "vc1"
    author = "Urs Liska"
    date = "2013-06-10"
    message = "This slur looks awful"
  } Slur
  d (
  cis )
}
